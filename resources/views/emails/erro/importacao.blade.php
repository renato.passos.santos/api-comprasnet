@component('mail::message')
# Importação de dados - API ComprasnetContratos

Houve algum erro na importação dos dados...

_Environment: {{ App::environment() }}_

@include('emails/common/assinatura')
@endcomponent
